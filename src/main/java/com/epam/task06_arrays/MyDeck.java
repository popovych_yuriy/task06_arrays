package com.epam.task06_arrays;

import java.util.ArrayList;
import java.util.List;

public class MyDeck {
    private List<Integer> deque = new ArrayList<Integer>();
    public void insertFront(int item){
        deque.add(0,item);
    }

    public void insertRear(int item){
        deque.add(item);
    }

    public void removeFront(){
        if(deque.isEmpty()){
            return;
        }
        int rem = deque.remove(0);
    }

    public void removeRear(){
        if(deque.isEmpty()){
            return;
        }
        int rem = deque.remove(deque.size()-1);
        System.out.println(deque);
    }

    public int peakFront(){
        int item = deque.get(0);
        return item;
    }

    public int peakRear(){
        int item = deque.get(deque.size()-1);
        return item;
    }
}
