package com.epam.task06_arrays.pqueue;

/*
Algorithm :
PUSH(HEAD, DATA, PRIORITY) This function is used to insert a new data into the queue.
Step 1: Create new node with DATA and PRIORITY
Step 2: Check if HEAD has lower priority. If true follow Steps 3-4 and end. Else goto Step 5.
Step 3: NEW -> NEXT = HEAD
Step 4: HEAD = NEW
Step 5: Set TEMP to head of the list
Step 6: While TEMP -> NEXT != NULL and TEMP -> NEXT -> PRIORITY > PRIORITY
Step 7: TEMP = TEMP -> NEXT
[END OF LOOP]
Step 8: NEW -> NEXT = TEMP -> NEXT
Step 9: TEMP -> NEXT = NEW
Step 10: End

POP(HEAD) This function removes the element with the highest priority form the queue.
Step 2: Set the head of the list to the next node in the list. HEAD = HEAD -> NEXT.
Step 3: Free the node at the head of the list
Step 4: End

PEEK(HEAD): This function is used to get the highest priority element in the queue without removing it from the queue.
Step 1: Return HEAD -> DATA
Step 2: End
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyPriorityQueue {
    private static Logger logger = LogManager.getLogger(MyPriorityQueue.class);
    private static class Node{
        int data;

        //Lower values indicate higher priority
        int priority;

        Node next;
    }

    private static Node node;

    //create new node
    public static Node newNode(int d, int p){
        Node temp = new Node();
        temp.data = d;
        temp.priority = p;
        temp.next = null;

        return temp;
    }

    //return the value at head
    public static int peek(Node head){
        return head.data;
    }

    //Removes element with
    //the highest priority from the list
    public static Node pop(Node head){
        Node temp = head;
        head = head.next;
        return head;
    }

    //Push according to priority
    public static Node push(Node head, int d, int p){
        Node start = head;

        Node temp = newNode(d, p);

        //If head of list has lesser priority than new node.
        //Inset new node before head node and change head
        if(head.priority > p){
            temp.next = head;
            head = temp;
        } else {
            //find position to insert new node
            while(start.next != null && start.next.priority < p){
                start = start.next;
            }
            temp.next = start.next;
            start.next = temp;
        }
        return head;
    }

    public static boolean isEmpty(Node head){
        return(head == null)?true:false;
    }

    public static void main(String args[]) {
        // Create a Priority Queue
        // 7.4.5.6
        Node pq = newNode(4, 1);
        pq = push(pq, 5, 2);
        pq = push(pq, 6, 3);
        pq = push(pq, 7, 0);

        while (!isEmpty(pq)) {
            logger.info(peek(pq));
            pq=pop(pq);
        }
    }
}
