package com.epam.task06_arrays.game;

import com.epam.task06_arrays.game.model.BusinessLogic;
import com.epam.task06_arrays.game.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
