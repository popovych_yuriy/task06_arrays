package com.epam.task06_arrays.game.view;

import com.epam.task06_arrays.game.controller.Controller;
import com.epam.task06_arrays.game.controller.ControllerImpl;
import com.epam.task06_arrays.game.model.Door;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    List<Door> doors;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print doors list");
        menu.put("2", "  2 - print number of fatal doors");
        menu.put("3", "  3 - successful walkthrough");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton3() {
        doors = controller.walkthrough();
        if (doors == null){
            logger.info("Mission Impossible");
        }else{
            logger.info("Successful walkthrough:");
            for (Door door: doors) {
                if (door.getPoint() > 0){
                    logger.info("Door " + door.getId() + ": artifact points " + door.getPoint());
                } else {
                    logger.info("Door " + door.getId() + ": monster power " + door.getPoint());
                }
            }
        }
    }

    private void pressButton2() {
        logger.info("Number of fatal doors: " + controller.deathDoors());
    }

    private void pressButton1() {
        doors = controller.getDoors();
        for (Door door: doors) {
            if (door.getPoint() > 0){
                logger.info("Door " + door.getId() + ": artifact points " + door.getPoint());
            } else {
                logger.info("Door " + door.getId() + ": monster power " + door.getPoint());
            }
        }
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }

}
