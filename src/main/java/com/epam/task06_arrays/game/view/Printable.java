package com.epam.task06_arrays.game.view;

@FunctionalInterface
public interface Printable {
    void print();
}
