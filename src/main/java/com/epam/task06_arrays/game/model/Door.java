package com.epam.task06_arrays.game.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Door {
    private static final AtomicInteger count = new AtomicInteger(0);
    private final int id;
    private final int points;

    public Door(int points) {
        this.points = points;
        id = count.incrementAndGet();
    }

    public int getPoint() {
        return points;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Door{" +
                "points=" + points +
                '}';
    }
}
