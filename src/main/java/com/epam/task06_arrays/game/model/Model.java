package com.epam.task06_arrays.game.model;

import java.util.List;

public interface Model {
    List<Door> getDoors();
    int deathDoors();
    List<Door> walkthrough();
}
