package com.epam.task06_arrays.game.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BusinessLogic implements Model {
    private int doorsAmount = 10;
    private List<Door> doors;
    private int artifactMin = 10;
    private int artifactMax = 80;
    private int monsterMin = 5;
    private int monsterMax = 100;
    private Random random;
    private final int heroPower = 25;

    public BusinessLogic() {
        random = new Random();
        doors = new ArrayList<>();
        for (int i = 0; i < doorsAmount; i++) {
            if(random.nextBoolean()){
                doors.add(new Door(random.nextInt(((artifactMax - artifactMin) + 1) + artifactMin)));
            }else{
                doors.add(new Door(- random.nextInt(((monsterMax - monsterMin) + 1) + monsterMin)));
            }
        }
    }

    public List<Door> getDoors() {
        return doors;
    }

    public int deathDoors(){
        int result = 0;
        for (Door door : doors) {
            if((heroPower + door.getPoint()) < 0){
                result++;
            }
        }
        return result;
    }

    public List<Door> walkthrough(){
        List<Door> result = new ArrayList<>();
        List<Door> monsters = new ArrayList<>();
        int actualPower = heroPower;
        for (int i = 0; i < doorsAmount; i++) {
            if(doors.get(i).getPoint() > 0){
                actualPower += doors.get(i).getPoint();
                result.add(doors.get(i));
            }else{
                monsters.add(doors.get(i));
            }
        }
        for (int i = 0; i < monsters.size() ; i++) {
            if(actualPower >=  (- monsters.get(i).getPoint())){
                result.add(monsters.get(i));
            }else{
                return null;
            }
        }
        return result;
    }
}
