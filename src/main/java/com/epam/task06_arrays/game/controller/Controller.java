package com.epam.task06_arrays.game.controller;

import com.epam.task06_arrays.game.model.Door;

import java.util.List;

public interface Controller {
    List<Door> getDoors();
    int deathDoors();
    List<Door> walkthrough();
}
