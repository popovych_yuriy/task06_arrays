package com.epam.task06_arrays.game.controller;

import com.epam.task06_arrays.game.model.BusinessLogic;
import com.epam.task06_arrays.game.model.Door;
import com.epam.task06_arrays.game.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<Door> getDoors() {
        return model.getDoors();
    }

    @Override
    public int deathDoors() {
        return model.deathDoors();
    }

    @Override
    public List<Door> walkthrough() {
        return model.walkthrough();
    }
}
