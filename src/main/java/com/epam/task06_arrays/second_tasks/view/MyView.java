package com.epam.task06_arrays.second_tasks.view;

import com.epam.task06_arrays.second_tasks.controller.Controller;
import com.epam.task06_arrays.second_tasks.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - add string to container");
        menu.put("2", "  2 - get string from container by index");
        menu.put("3", "  3 - get array");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton3() {
        logger.info("Full array:");
        String [] array = controller.getArray();
        for (String line : array) {
            logger.info(line);
        }
    }

    private void pressButton2() {
        logger.info("Please type index:");
        int index = input.nextInt();
        logger.info(controller.getString(index));
    }

    private void pressButton1() {
        logger.info("Please type any text:");
        String line = input.nextLine();
        controller.putSpting(line);
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
//                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
