package com.epam.task06_arrays.second_tasks.view;

@FunctionalInterface
public interface Printable {

    void print();
}
