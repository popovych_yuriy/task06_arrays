package com.epam.task06_arrays.second_tasks;

import com.epam.task06_arrays.second_tasks.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
