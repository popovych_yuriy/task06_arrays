package com.epam.task06_arrays.second_tasks.controller;

public interface Controller {
    void putSpting(String line);
    String getString(int index);
    String[] getArray();
}
