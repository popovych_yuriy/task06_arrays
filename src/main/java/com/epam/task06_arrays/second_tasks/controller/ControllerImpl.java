package com.epam.task06_arrays.second_tasks.controller;

import com.epam.task06_arrays.second_tasks.model.BusinessLogic;
import com.epam.task06_arrays.second_tasks.model.Model;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void putSpting(String line) {
        model.putSpting(line);
    }

    @Override
    public String getString(int index) {
        return model.getString(index);
    }

    @Override
    public String[] getArray() {
        return model.getArray();
    }
}