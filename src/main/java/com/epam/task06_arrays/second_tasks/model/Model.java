package com.epam.task06_arrays.second_tasks.model;

public interface Model {
    void putSpting(String line);
    String getString(int index);
    String[] getArray();
}
