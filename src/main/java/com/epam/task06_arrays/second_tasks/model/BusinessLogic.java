package com.epam.task06_arrays.second_tasks.model;

public class BusinessLogic implements Model {
    ArrayContainer arrayContainer;

    public BusinessLogic() {
        arrayContainer = new ArrayContainer();
    }
    public String getString(int index){
        return arrayContainer.getString(index);
    }

    public void putSpting(String line) {
        arrayContainer.putSpting(line);
    }

    public String[] getArray() {
        return arrayContainer.getStrings();
    }
}
