package com.epam.task06_arrays.second_tasks.model;

import java.util.Arrays;

public class ArrayContainer {
    private String strings [];

    public ArrayContainer() {
        this.strings = new String[0];
    }

    public String getString(int index) {
        String line = strings[index];
        return line;
    }

    public void putSpting(String line) {
        strings = Arrays.copyOf(strings, strings.length + 1);
        strings[strings.length - 1] = line;
    }

    public String[] getStrings() {
        return strings;
    }

    //    private void increase(){
//        String[] temp = new String[strings.length + 1];
//        for (int i = 0; i < strings.length; i++){
//            temp[i] = strings[i];
//        }
//        strings = temp;
//    }
}
