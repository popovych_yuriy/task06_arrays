package com.epam.task06_arrays.generics;

public class Unit {
    int unitNumber;

    public Unit(int unitNumber) {
        this.unitNumber = unitNumber;
    }

    public int getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(int unitNumber) {
        this.unitNumber = unitNumber;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "unitNumber=" + unitNumber +
                '}';
    }
}
