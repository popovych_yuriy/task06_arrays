package com.epam.task06_arrays.generics;

public class UnitsCarrier<T extends Unit> {
    private T unit;

    public void loadUnit(T unit){
        this.unit = unit;
    }
    public T uploadUnit(){
        return this.unit;
    }
}
