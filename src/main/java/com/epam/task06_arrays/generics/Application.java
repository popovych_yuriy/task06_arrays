package com.epam.task06_arrays.generics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        UnitsCarrier<Unit> carrier = new UnitsCarrier<>();
        Unit unit;

        unit = new Unit(123);
        carrier.loadUnit(unit);
        Unit carriedUnit = carrier.uploadUnit();
        logger.info("CarriedUnit: "+carriedUnit);

        unit = new Unit(42);
        carrier.loadUnit(unit);
        Unit carriedSecondUnit = carrier.uploadUnit();
        logger.info("CarriedSecondUnit: "+carriedSecondUnit);

        List <Unit> units = new ArrayList<>();
        units.add(carriedUnit);
        units.add(carriedSecondUnit);
        List <?> unitsWildcard = units;
        for (Object x : unitsWildcard) {
            logger.info("Print wildcard list: "+x);
        }
    }
}
