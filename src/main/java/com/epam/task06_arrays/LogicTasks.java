package com.epam.task06_arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class LogicTasks {
    private static Logger logger = LogManager.getLogger(LogicTasks.class);
    private int [] first = new int [] {1, 2, 3, 4, 5};
    private int [] second = new int [] {1, 3 , 5, 7, 9};
    public static void main(String[] args) {
        LogicTasks tasks = new LogicTasks();
        logger.info("First array: " + Arrays.toString(tasks.first));
        logger.info("Second array: " + Arrays.toString(tasks.second));

        //task Aa - similar elements of arrays
        logger.info("Similar elements: " + Arrays.toString(tasks.similar()));

        //task Ab - different elements of arrays
        logger.info("Different elements: " + Arrays.toString(tasks.different(tasks.similar())));

        //task B - delete elements in the array that repeats more than 2 times
        int [] third = new int [] {1, 3 , 5, 7, 9, 5, 6, 5, 5, 6, 6};
        logger.info("Array: " + Arrays.toString(third));
        logger.info("Array without elements that repeatet more then 2 times: " + Arrays.toString(tasks.delete(third)));

        //task C - find and delete sequences of equal elements except first one
        logger.info("Array: " + Arrays.toString(third));
        logger.info("Array without sequences of equal elements: " + Arrays.toString(tasks.deleteSequences(third)));
    }

    private int[] deleteSequences(int[] input) {
        int saveInt = 0;
        int index = 0;
        for (int i = 0; i < input.length; i++) {
            if (i == 0) {
                saveInt = input[i];
                index++;
                continue;
            }
            if (saveInt == input[i]) {
            } else {
                index++;
            }
            saveInt = input[i];
        }
        int[] result = new int[index];
        index=0;
        for (int i = 0; i < input.length; i++) {
            if (i == 0) {
                saveInt = input[i];
                continue;
            }
            if (saveInt == input[i]) {

            } else {
                result[index++] = saveInt;
            }
            saveInt = input[i];

            if(i+1 == input.length){
                result[index] = input[i];
            }
        }
        return  result;
    }

    private int[] delete(int[] input) {
        int saveInt = 0;
        int index = 0;
        int indexArrayLength = 0;
        for (int i = 0; i < input.length; i++) {
            saveInt = input[i];
            index = 0;
            for (int j = 0; j < input.length; j++) {
                if (saveInt == input[j]) {
                    index++;
                }
            }
            if (index < 3) {
                indexArrayLength++;
            }
        }
        int[] result = new int[indexArrayLength];

        indexArrayLength = 0;
        for (int i = 0; i < input.length; i++) {
            saveInt = input[i];
            index = 0;
            for (int j = 0; j < input.length; j++) {
                if (saveInt == input[j]) {
                    index++;
                }
            }
            if (index < 3) {
                result[indexArrayLength++] = input[i];
            }
        }
        return result;
    }

    private int[] similar(){
        int [] result = new int[0];
        for (int i = 0; i < first.length ; i++) {
            for (int j = 0; j < second.length ; j++) {
                if (first[i] == second[j]) {
                    result = increaseArray(result);
                    result[0] = first[i];
                }
            }
        }
        return result;
    }

    private int[] different(int[] similar) {
        int  [] result = new int[0];

        for (int i = 0; i < first.length ; i++) {
            if (!contains(similar, first[i])){
                result = increaseArray(result);
                result[0] = first[i];
            }
        }
        for (int i = 0; i < second.length ; i++) {
            if (!contains(similar, second[i])){
                result = increaseArray(result);
                result[0] = second[i];
            }
        }
        return result;
    }
    private boolean contains(int[] array, int v) {
        boolean result = false;
        for(int i : array){
            if(i == v){
                result = true;
                break;
            }
        }
        return result;
    }

    private int [] increaseArray (int [] input){
        int [] result = new int [input.length +1];
        System.arraycopy(input, 0, result, 1, input.length);
        return result;
    }
}
